# TABLESCI

A _simple_, not-slow language for dealing with numbers, simple formulae,
and tables.

(There's no precise definition of not-slow yet.)


## Status

This is strictly a spare-time project. No spare time will mean no progress.

Ironically, there's zero support for tables yet. I might not end up adding any
in here.

But the language's complete-ish in the sense that it has the constructs it
needs.

Arithmetic expressions, recursive functions, conditionals, function and
variable definitions all work. Operators need a little love but I don't actually
root for their use much anyway.

More built in data structures (tables!) and functions are a priority. We can't
even do anything with strings for now. But this is all the easy part.

Other nice-to-haves like REPL support and things will be added slowly.


## Implementation notes

We just build on top of the Racket language, and the Beautiful Racket and Brag
libraries. It's really not much code at the end of the day, very little work
overall. Racket's _cool_ for this stuff.


## The language

So, programs in this language are meant to be written like a solution to a textbook
math problem. You write a bunch of "let" definitions, where the "let" is an optional
keyword, and the definition may be an arbitrary expression or function, followed by
a series of expression to produce whatever answers you needed.

A function is a similar thing, btw: A series of definitions followed by ONE
expression.

There's conditionals and recursion.

There's NO mutation. Also, NO shadowing bindings.

There's lists, maps (dictionaries) and anonymous functions.
Functions are first-class citizens.

And there's line comments.

Commas are optional in lists and maps. Also, newlines are often significant! But
generally programs are meant to run OK if they look OK.


All variables names are either single-letter lower-case characters, in which case
they're meant to be used in simple arithmetic expressions, or they start
with a capital letter. This distinction exists so we can right `xy + Sin(x)` and
stuff.

That's about it; here's what it looks like:

```
#lang tablesci

;; This is a comment.

x := 2
let y := 3
z := [1, 2, 3]
Lst := [1
        2
        3]

d := {"a": 1}
Dct := {"a": 2
        "b": 3}

let Id(a) := a

Abs(n) :=
  case { n > 0: n
         else: n * -1 }
Fib(n) :=
  case {
    n = 0: 0
    n = 1: 1
    else: Fib(n-1)+Fib(n-2)
  }

Anon := \a,b -> (a + b)

Bigger(a, b) := case {
    a > b: a
    else: b
}

xy + Bigger(5, 6)
```

Enjoy.
